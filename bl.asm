jmp short entrypt	; skip BPB to entrypt
nop

; lines up to  entrypt  describe the dimensions of 1.44MB 3.5" floppy
OEMid		db "MSWIN4.1"
BytesPerSector	dw 512
SecsPerCluster	db 1
ReserveCount	dw 1
FATcount	db 2
DirEntries	dw 224
TotalSectors	dw 2880
MediaDescriptor	db 0xF0
SectorsPerFAT	dw 9
SectorsPerTrack	dw 18
HeadCount	dw 2
HiddenSectors	dd 0
dd 0
DriveNumber	db 0
DirtyBit	db 1
extBootSig	db 0x29
VolumeID	dd 77
VolumeLabel	db "JFos       ", 0
FSTYPE		db "FAT12   "

entrypt:

	mov ax, 0x07c0	; put ds @ 07c0h:0h
	mov ds, ax
	cld

	mov si, startup	; print successful startup indicator
	call putch

	xor ax, ax	; rst medium 3 times
	int 0x13
	mov si, disk_reset	; put rst medium indicator
	call putch
	xor ax, ax
	int 0x13
	mov si, disk_reset
	call putch
	xor ax, ax
	int 0x13
	mov si, disk_reset
	call putch

	mov si, load_data	; print data load indicator
	call putch

	xor ax, ax
	mov es, ax
	mov bx, 0x0A00	; load @ ES:BX = 0x0000:0x0A00
	mov ah, 2	; req by int13h
	mov al, 1	; # of sectors to load
	mov ch, 0	; cylinder #
	mov cl, 17	; sector #
	mov dh, 1	; head & drive #
			; chs = 0/1/17
	int 0x13	; invoke load interrupt
	jc halt		; if CF=1 after int13h then jump to halt
	
	mov si, data_loaded	; print successful load data indicator
	call putch

	mov si, lf	; print CRLF
	call putch
	mov si, cr
	call putch

	jmp 0x0000:0x0A00	; jump to init program (default: JIN)

	hlt		; if anything goes wrong then halt

putch:
	lodsb		; load byte from SI into AL
	mov ah, 0x0e	; reqired by int10h
	mov bh, 0	; text page 0
	int 0x10	; invoke int10h
	ret		; return call

halt:
	hlt

startup: db "!"
disk_reset: db "."
load_data: db "l"
data_loaded: db "L"
cr: db 0x0d
lf: db 0x0a


times 510-($-$$) db 0
dw 0xAA55
