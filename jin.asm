	mov ax, 0xA0		; set ds @ 0x00A0:0x0000 (address jumped to from bl.sys)
	mov ds, ax

	mov ah, 0x0e		; print successful start indicator
	mov al, "!"
	int 0x10

	mov ax, 0x50		; setup stack @ 0x0050:0x0000 with SP @ 0x0050:0x00ff (255 byte stack)
	mov ss, ax
	mov sp, 0xff
	
	xor bx, bx		; setup IVT entry 64h with interrupt handler @ 0x0060:0x0000
	mov es, bx		; IVT 64h = 0x0000:0x0190
	mov bx, 0x0190
	mov byte [es:bx], 0	; set offset part of interrupt handler address
	add bx, 2		; offset by 2 bytes to set segment part
	mov byte [es:bx], 0x60	; set segment part of interrupt handler address
	
	mov ah, 0x0e		; print Interrupt Set indicator
	mov al, "i"
	int 0x10

	xor ax, ax		; reset disk three times
	int 0x13
	mov ah, 0x0e
	mov al, "."
	int 0x10
	xor ax, ax
	int 0x13
	mov ah, 0x0e
	mov al, "."
	int 0x10
	xor ax, ax
	int 0x13
	mov ah, 0x0e
	mov al, "."
	int 0x10

	mov ax, 0x60		; load int64h handler @ 0x0060:0x0000
	mov es, ax
	xor bx, bx
	mov ah, 2
	mov al, 1		; 1 sector
	mov ch, 0		; cylinder #
	mov cl, 18		; sector #
	mov dh, 1		; head #
	int 0x13		; chs 0/1/18
	jc halt			; if CF=1 then halt

	mov ah, 0x0e		; print Interrupt Handler Loaded indicator
	mov al, "I"
	int 0x10

	mov ah, 0x0e		; print Prepare Shell Load indicator
	mov al, "s"
	int 0x10

	xor ax, ax		; reset disk 3 times
	int 0x13
	mov ah, 0x0e
	mov al, "."
	int 0x10
	xor ax, ax
	int 0x13
	mov ah, 0x0e
	mov al, "."
	int 0x10
	xor ax, ax
	int 0x13
	mov ah, 0x0e
	mov al, "."
	int 0x10

	mov ax, 0xE0		; load JFshell @ 0x00e0:0x0000
	mov es, ax
	xor bx, bx
	mov ah, 2
	mov al, 1		; 1 sector
	mov ch, 1		; cylinder #
	mov cl, 1		; sector #
	mov dh, 0		; head #
	int 0x13		; chs 1/0/1
	jc halt			; if CF=1 then halt

	mov ah, 0x0e		; print JFshell Loaded indicator
	mov al, "S"
	int 0x10

	mov ah, 2		; use int64h subroutine 02h to print JIN init program info
	mov bx, 0xa0		; print string from 0x00a0:jin_info
	mov es, bx
	mov bx, jin_info
	int 0x64		; first invocation of int64h in whole OS

	jmp 0x00e0:0x0000	; jump to JFshell

halt:
	hlt

jin_info: db 0x0a, 0x0a, 0x0d, "JIN init program", 0x0a, 0x0d, "(c) 2019 Papa Vince", 0x0a, 0x0d, "v0.1", 0x0a, 0x0d, 0
