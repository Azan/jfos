	mov ax, 0x00E0
	mov ds, ax	; set up DS to 0xE0
	mov es, ax      ; set up ES to 0xE0, needed by int 64h

	mov ah, 2	; print program information (int 64h _print)
	mov bx, jfshell_info
	int 0x64

shell_main:
	mov ah, 2	; print command prompt sequence "?> "
	mov bx, prompt
	int 0x64

	;call getline		; request line from keyboard
	;mov byte [kbdcnt], 0	; TODO: migrate to int 64h
	;mov cx, 63
	;L1:
	;mov bx, cx
	;mov byte [kbdbuf + bx], 0
	;loop L1

	mov ah, 3	; invoke int 64h _kbd_getline
	int 0x64

	mov ah, 2	; print CRLF (int 64h _print)
	mov bx, crlf
	int 0x64

	jmp shell_main	; loop back to get next line

jfshell_info: db 0x0a, 0x0d, "JFshell FORTH interpreter & system shell", 0x0a, 0x0d, "(c) 2019 Papa Vince", 0x0a, 0x0d, "v0.1", 0x0a, 0x0d, 0
prompt: db "?> ", 0
crlf: db 0x0a, 0x0d, 0
