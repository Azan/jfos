	pusha		; push all registers to stack
	push es		; push ES register to stack

	cmp ah, 0	; subroutine 00h - halt & disable interrupts
	jz _halt

	cmp ah, 1	; subroutine 01h - putchar
	jz _putch

	cmp ah, 2	; subroutine 02h - print string
	jz _print

	cmp ah, 3	; subroutine 03h - input line from keyboard (not done yet)
	jz _kbd_getline

	mov bx, 0x60	; if none match then set ds @ 0x0060:0x0000
	mov ds, bx

	mov si, fatal_error	; put address of  fatal_error  string into SI
	call __print		; call internal string print function

	cli			; disable interrupts
	hlt			; halt processor

fatal_error: db 0x0a, 0x0d, "Absurd request: Invalid AH", 0x0a, 0x0d, 0
kbdover: db 0x0a, 0x0d, "kbdbuf overflow, flushing...", 0x0a, 0x0d, 0

_halt:
	cli
	hlt

_putch:
	mov ah, 0x0e	; utilise int10h to print a character
	mov bh, 0	; TODO: make independent of bios interrupts
	int 0x10
	xor ax, ax
	pop es		; pop ES register
	popa		; pop all registers
	iret		; return from interrupt

;_putch:
;	push ax
;	xor ax, ax
;;	xor edx, edx
;	mov al, 0x0f
;	out 0x3d4, al
;	in al, 0x3d5
;	or dx, ax
;	mov ax, 0x0e
;	out 0x3d4, al
;	xor ax, ax
;	in al, 0x3d5
;;	or dx, ax
;	shl dx, 8
;	mov ebx, 0xb8000
;;	add ebx, edx
;	pop ax
;	mov byte [ebx], al
;	ret
	

_print:
	mov al, byte [es:bx]	; move byte from ES:BX to al
	or al, al		; if AL || AL == 0 then call _return
	jz _return
	call __putch		; if AL || AL != 0 then call __putch (internal character print)
	inc bx			; increase BX to receive next character from ES:BX
	jmp _print		; repeat until AL || AL == 0

_kbd_getline:			; !done yet
	mov bx, 0x9c		; supposed keyboard buffer @ 0x009c:0x0000
	mov es, bx
	xor bx, bx
	mov cx, 63		; loop counter
	L0_getline:
		xor ax, ax	; clear AX
		int 0x16	; read char from keyboard
		cmp ah, 29	; if kbd scan code == 29 then return from interrupt
		jz _return
		cmp ah, 42	; if kbd scan code == 42 or 43 then -//-
		jz _return
		cmp ah, 43
		jz _return
		mov byte [es:bx], al	; move ascii char from al to ES:BX
		inc bx			; increase BX to point @ next memory cell
		call __putch		; print char in AL
	loop L0_getline			; loop to L0_getline 63 times
	push ds				; if CX == 0 then push DS
	mov bx, 0x60			; set DS @ 0x0060:0x0000 (top of program)
	mov ds, bx
	mov si, kbdover			; move address of kbdover message to SI
	call __print			; print from SI
	pop ds
	jmp _return

_return:
	pop es
	popa
	iret

__putch:		; internal character print
	mov ah, 0x0e
	mov bh, 0
	int 0x10
	ret

__print:		; internal string print
	lodsb
	or al, al
	jz __return
	call __putch
	jmp __print

__return:		; internal return
	ret

